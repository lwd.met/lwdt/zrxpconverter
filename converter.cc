/***********************************************************************************/
/* Copyright 2019 Lawinenwarndienst Tirol / Avalanche Warning Service Tyrol        */
/***********************************************************************************/
/* This is free software: you can redistribute and/or modify it under the terms of */
/* the GNU Lesser General Public License 3 or later: http://www.gnu.org/licenses   */
/***********************************************************************************/

/* SYNOPSIS: ./converter <start_date> <end_date> */

/*
 * MeteoIO control program to import sub-optimal WISKI "csv" format and convert to SMET.
 * Michael Reisecker, 2019-04
 */

#include <meteoio/MeteoIO.h>

#include <iostream>
#include <string>

using namespace std;
using namespace mio; //MeteoIO's namespace

bool has_nodata(const MeteoData md) { //does a data point contain any nodata values?
	for (size_t pp = 0; pp < md.nrOfParameters; ++pp) {
		if (md(pp) == IOUtils::nodata)
			return true;
	}
	return false;
}

int main(int argc, char** argv)
{
	string start_date, end_date;
	if (argc == 1) {
		start_date = "1900-01-01";
		end_date = "2200-01-01";
	} else if (argc == 3) {
		start_date = argv[1];
		end_date = argv[2];
	} else {
		throw invalid_argument("Unexpected number of input arguments.");
	}

	const string ini_path("input/io.ini");
	Config cfg(ini_path);
	IOManager io(cfg);
	
	const double TZ = cfg.get("TIME_ZONE", "Input");
	Date sdate, edate;
	IOUtils::convertString(sdate, start_date, TZ);
	IOUtils::convertString(edate, end_date, TZ);

	vector< vector<MeteoData> > mvec;

	cout << "*** Converting dates " << sdate.toString(Date::ISO) << " to " << edate.toString(Date::ISO) << "... " << std::flush;

	io.getMeteoData(sdate, edate, mvec);

	Meteo1DInterpolator interpol(cfg);
	MeteoData md;

	for (size_t ii = 0; ii < mvec.size(); ++ii) {
		const std::string stationHash( IOUtils::toString(ii) + "-" + mvec[ii].front().meta.getHash() );
		for (size_t jj = 0; jj < mvec[ii].size(); ++jj) {
			if ( has_nodata(mvec[ii][jj]) ) { //interpolate if any data point at this time step is nodata
				interpol.resampleData(mvec[ii][jj].date, stationHash, mvec[ii], md);
				mvec[ii][jj] = md;
			}
		} //endfor jj
	}

	io.writeMeteoData(mvec);

	cout << "done." << endl;

	const double sampling_rate = 1. / (io.getAvgSamplingRate()*60);
	if (sampling_rate < 0)
		cout << "[W] No valid data found for time span." << endl;

	return 0;
}

