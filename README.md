# ZRXP Converter

Helper project to read sub-optimal WISKI csv files and convert the data to SMET.

There is a small wiki available at this project's git repository:

https://gitlab.com/lwd.met/lwdt/zrxpconverter/wikis/home